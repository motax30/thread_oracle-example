package com.fatec.garcia.thread_example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static Float getSaldo(final Connection con, final Integer idCustomer) {
		Statement stmt;
		Float saldo = 0f;
		try {
			stmt = con.createStatement();
			final String query = "select cust_credit_limit from sh.customers where cust_id=" + idCustomer;
			final ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				saldo = Float.parseFloat(rs.getString("cust_credit_limit"));
			}
			stmt.close();
		} catch (final SQLException e) {
			e.printStackTrace();
		}
		return saldo;
	}

	public static List<Integer> getIdCustomers(final Connection con) throws SQLException {
		final Statement stmt = con.createStatement();
		final List<Integer> listIdsCustomers = new ArrayList<>();
		final String idsCustomers = "select cust_id from sh.customers";
		final ResultSet rs = stmt.executeQuery(idsCustomers);
		while (rs.next()) {
			final String a = rs.getString("cust_id");
			listIdsCustomers.add(Integer.parseInt(a));
		}
		return listIdsCustomers;
	}

	public static void executarUpdate(final Connection con, final Integer id_customer, final String pathArquivo,
			final Float saldo) {
		Statement stmt = null;
		try {
			stmt = con.createStatement();
		} catch (final SQLException e1) {
			e1.printStackTrace();
		}

		final String name = Thread.currentThread().getName();
		// Inserting data in database
		final String q1 = "update sh.customers SET cust_credit_limit = cust_credit_limit-100 where cust_id="
				+ id_customer;
		String lock = "lock table sh.customers in exclusive mode";

		int x = 0;
		int y = 0;
		try {
			if(TIPO_ISOLATION.equals(Connection.TRANSACTION_SERIALIZABLE)){
				y = stmt.executeUpdate(lock);
				con.commit();
			}
			x = stmt.executeUpdate(q1);
			con.commit();
			Calendar data;
			if (x > 0 || y>0) {
				data = Calendar.getInstance();
				final int horas = data.get(Calendar.HOUR_OF_DAY);
				final int minutos = data.get(Calendar.MINUTE);
				final int segundos = data.get(Calendar.SECOND);
				final int milisegundos = data.get(Calendar.MILLISECOND);
				final String mensagem = name + ";" + horas + ";" + minutos + ";" + segundos + ";" + milisegundos + ";"
						+ saldo + ";" + id_customer;
				escreverNoArquivo(pathArquivo, mensagem);
				System.out.println(mensagem);
				stmt.close();
			} else {
				System.out.println("Update Failed");
			}
		} catch (final SQLException e) {
			e.printStackTrace();
		}
	}

	public static void encerrarConexao(final Connection con) throws SQLException {
		con.close();
	}

	Float saldo;

	public static void inicializarThreads(final Connection c) throws SQLException, InterruptedException {
		// Float saldo = 0f;
		for (int i = 0; i < 5; i++) {
			// saldo = getIdCustomers(c, ID_CUSTOMER);
			// if (saldo <= 100f) {
			// break;
			// }
			/* executarThread(c); */
			System.out.println("Executou a " + i + "Thread");
		}
	}

	public static Connection conectarBD(final int tipoIsolation) {
		Connection con = null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");

			// Establishing Connection

			con = DriverManager.getConnection(
					// Entrar no Oracle por meio do cliente SqlDeveloper
					// e setar uma senha para o usuário "SH", no meu caso aqui a senha vai ser
					// "fatec"
					"jdbc:oracle:thin:@localhost:1521:orcl", "SH", "fatec");
			con.setTransactionIsolation(tipoIsolation);
			con.setAutoCommit(false);
			System.out.println("Connected");
		} catch (final Exception e) {
			System.out.println(e);
		}
		return con;
	}

	public static void escreverNoArquivo(final String path, final String texto) {
		try {
			// O parametro é que indica se deve sobrescrever ou continua no
			// arquivo.
			final FileWriter fw = new FileWriter(path, true);
			final BufferedWriter conexao = new BufferedWriter(fw);
			conexao.write(texto);
			conexao.newLine();
			conexao.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public static void escrever(final FileWriter arq, final String mensagem) throws IOException {
		final PrintWriter gravarArq = new PrintWriter(arq);
		for (int i = 1; i <= 10; i++) {
			gravarArq.printf(mensagem); // Aqui acontece a escrita em cada linha no arquivo
		}
		arq.close();
	}

	private static Runnable t1 = new Runnable() {
		public void run() {
			final Connection con = conectarBD(TIPO_ISOLATION);
			Float saldo;
			try {
				final List<Integer> ids_customers = getIdCustomers(con);
				for (final Integer idCustomer : ids_customers) {
					saldo = 200f;
					while (saldo > 100) {
						// if (saldo > 100f) {
						executarUpdate(con, idCustomer, pathArquivo, saldo);
						saldo = getSaldo(con, idCustomer);
						// }
					}
				}
				encerrarConexao(con);
			} catch (final Exception e) {
			}

		}
	};

	static String pathArquivo;
	
	//Alterar o tipo de isolamento para os arranjos de execução de 1,5,10,15 e 20 Threads
	static Integer TIPO_ISOLATION = Connection.TRANSACTION_SERIALIZABLE;

	public static void main(final String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		pathArquivo = "lab_thread_concorrencia_20_thread_serializable.txt";
		for(int i=1; i <= 20 ;i++){
			new Thread(t1,"TH"+i).start();
		}
	};

}
